# Programação utilizando o microcontrolador PIC16F887

Esboço de material de apoio às aulas de microcontrolador
Autor: José William Rodrigues Pereira

A proposta é desenvolver material de apoio e consulta para os alunos
que estudam microcontroladores da família PIC.

Itens utilizados:
 - Microcontrolador PIC16F887;
 - MPLAB-X;
 - XC8;
 - SimulIDE;
 - Linguagem C.

