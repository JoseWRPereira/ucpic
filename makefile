TEXTO=./texTexto/
SLIDES=./texSlides/
MAIN=main
LATEX=pdflatex
PDFVIEWER=evince


dissertacao: PDFLATEX
	$(PDFVIEWER) $(MAIN).pdf&

PDFLATEX: LATEX1
	$(LATEX) -synctex=1 -interaction=nonstopmode $(TEXTO)$(MAIN)


LATEX1: NOMENCLATURA BIB
	$(LATEX) $(TEXTO)$(MAIN)

BIB: LATEX0
	bibtex $(MAIN)

NOMENCLATURA: LATEX0 $(MAIN).nlo
	makeindex $(MAIN).nlo -s nomencl.ist -o $(MAIN).nls

LATEX0: $(TEXTO)$(MAIN).tex $(TEXTO)configPacks.sty
	$(LATEX) $(TEXTO)$(MAIN) 


clean:
clear:
	rm -f *.aux
	rm -f ./texSlides/*.aux
	rm -f *.bbl
	rm -f *.bcf
	rm -f *.blg
	rm -f *.ilg
	rm -f *.lof
	rm -f *.log
	rm -f *.lot
	rm -f *.nav
	rm -f *.nlo
	rm -f *.nls
	rm -f *.out
	rm -f *.pdf
	rm -f *.run.xm
	rm -f *.run.xml
	rm -f *.toc
	rm -f *.synctex.gz
	rm -f *.snm
	rm -f *~
	rm -f ./texTexto/*.aux
	rm -f ./texTexto/*.fdb_latexmk
	rm -f ./texTexto/*.fls
	rm -f ./texTexto/*.log	
	rm -f ./texTexto/*.nlo
	rm -f ./texTexto/*.bst
	
